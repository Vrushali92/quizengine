//
//  AppDelegate.swift
//  QuizEngine
//
//  Created by Vrushali Kulkarni on 30/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

