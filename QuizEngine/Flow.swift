//
//  Flow.swift
//  QuizEngine
//
//  Created by Vrushali Kulkarni on 30/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import Foundation

protocol Router {
    typealias AnswerCallBack = (String) -> Void

    func routeTo(question: String,
                 answerCallBack: @escaping AnswerCallBack)

    func routeTo(result: [String:String])
}

class Flow {

    private let router: Router
    private let questions: [String]

    private var result: [String:String] = [:]

    init(questions: [String], router: Router) {
        self.router = router
        self.questions = questions
    }

    func start() {
        if let firstQuestion = questions.first {
            router.routeTo(question: firstQuestion,
                           answerCallBack: nextCallBack(from: firstQuestion))
        } else {
            router.routeTo(result: result)
        }
    }

    private func nextCallBack(from question: String) -> Router.AnswerCallBack {
        return { [weak self] in self?.routeNext(question, $0) }
    }

    private func routeNext(_ question: String, _ answer: String) {

        guard let nextQuestionIndex = questions.firstIndex(of: question) else { return }
        result[question] = answer
        let index = nextQuestionIndex + 1
        if index < questions.count {
            let nextQuestion = questions[index]
            router.routeTo(question: nextQuestion,
                                      answerCallBack: nextCallBack(from: nextQuestion))
        } else {
            router.routeTo(result: result)
        }
    }
}
